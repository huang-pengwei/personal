import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.*;
import org.jsoup.select.*;

public class Score {

    public static void main(String[] args) throws IOException {
    	
    	
    	if (args.length < 2) {
    		System.out.println("程序需要两个html文件参数，请重试");
    		System.exit(0);
    	}
    	
    	// 解析网页文件，并合并数据统计结果
        Score score = new Score();        
        HashMap<String, Integer> small = score.parseHtml(args[0]);
        HashMap<String, Integer> all = score.parseHtml(args[1]);
        small.forEach((key, value) -> all.merge(key, value, (v1, v2) -> v1 + v2));

        // 读取配置文件,并配合公式计算总分
        score.calcScore(all);

    }
    
    // 计算
    private void calcScore(HashMap<String, Integer> all) throws IOException {
    	Properties pro = new Properties();
        pro.load(new FileInputStream(new File("total.properties")));
        int befores, bases, tests, programs, adds;
        befores = Integer.parseInt(pro.getProperty("before"));
        bases = Integer.parseInt(pro.getProperty("base"));
        tests = Integer.parseInt(pro.getProperty("test"));
        programs = Integer.parseInt(pro.getProperty("program"));
        adds = Integer.parseInt(pro.getProperty("add"));
        int before = all.get("before");
        int base = all.get("base");
        int test = all.get("test");
        int program = all.get("program");
        int add = all.get("add");
        double gredit = 0;
        gredit += before * 1.0 / befores * 100 * 0.25;
        gredit += base * 1.0 / bases * 95 * 0.3;
        gredit += test * 1.0 / tests * 100 * 0.2;
        gredit += program * 1.0 / programs * 95 * 0.1;
        gredit += add * 1.0 / adds * 90 * 0.05;
        int t = (int) (gredit * 100);
        System.out.println((double) t / 100 + 6);
    }

    // 解析结构
    private HashMap<String, Integer> parseHtml(String html) {
        HashMap<String, Integer> hashMap = new HashMap<>();
        String[] strings = {"before", "base", "test", "program", "add"};
        for (String string : strings) {
            hashMap.put(string, 0);
        }

        try {
            Document document = Jsoup.parse(new File(html), "UTF-8");
            Elements select = document.select(".interaction-name");
            for (Element e : select) {
                if (e.attr("title").contains("课堂小测")) {
                    countScore(e, hashMap, "test");
                } else if (e.attr("title").contains("编程题")) {
                    countScore(e, hashMap, "program");
                } else if (e.attr("title").contains("课堂完成部分")) {
                    countScore(e, hashMap, "base");
                } else if (e.attr("title").contains("附加题")) {
                    countScore(e, hashMap, "add");
                } else if (e.attr("title").contains("课前自测")) {
                    countScore(e, hashMap, "before");
                }
            }
        } catch (IOException e) {
        	System.out.println("系统找不到指定的文件:" + html + "。");
        	System.out.println("请检查文件名或文件路径是否正确!");
        	System.exit(0);
        }
        return hashMap;
    }
    
    // 解析分数
    private void countScore(Element e, HashMap<String, Integer> hashMap, String key) {
        int temp = 0;
        String text = e.parent().lastElementSibling().child(0).select("span:contains(经验)").select("span[style=color:#8FC31F;]").text();
        Matcher matcher = Pattern.compile("\\d+").matcher(text);
        while (matcher.find()) {
            String group = matcher.group();
            temp += Integer.parseInt(group);
        }
        hashMap.put(key, hashMap.get(key) + temp);
    }
}

